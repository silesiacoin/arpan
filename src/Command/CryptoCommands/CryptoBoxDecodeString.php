<?php
declare(strict_types=1);

namespace App\Command\CryptoCommands;

use Silesiacoin\Phplib\Sodium\SecretBox;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

class CryptoBoxDecodeString extends ContainerAwareCommand
{
    /** @var SymfonyStyle */
    private $io;

    /**
     * UserAdd constructor.
     * @throws \Exception
     */
    public function __construct()
    {
        parent::__construct('sodium:secretbox:decode');
    }

    protected function configure() : void
    {
        $this
            ->addOption(
                'Nonce',
                null,
                InputOption::VALUE_REQUIRED,
                'provide filename'
            )
            ->addOption(
                'Password',
                null,
                InputOption::VALUE_REQUIRED,
                'provide hashed password'
            )
            ->addOption(
                'FileName',
                null,
                InputOption::VALUE_REQUIRED,
                'Provide filename',
                'temp/secret.ssc'
            )
            ->addOption(
                'Bits',
                null,
                InputOption::VALUE_OPTIONAL,
                'provide Bit size',
                16
            )
            ->setDescription('provide nonce')
            ->setHelp('This command allows you to decode existing box with credentials');
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     * @return int|null|void
     * @throws \Symfony\Component\DependencyInjection\Exception\ServiceNotFoundException
     * @throws \Symfony\Component\DependencyInjection\Exception\ServiceCircularReferenceException
     * @throws \LogicException
     * @throws \InvalidArgumentException
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $hasher = new SecretBox(
            $input->getOption('Password'),
            $input->getOption('Nonce'),
            $input->getOption('Bits')
        );

        if (file_exists($input->getOption('FileName'))) {
            $hasher->setBinMessage($hasher->decodeBinToStr(file_get_contents($input->getOption('FileName'))));
        } else {
            $output->writeln('Provided file ' .$input->getOption('FileName') . ' not found');
            exit(404);
        }

        $hasher->decodeString();
        $output->writeln($hasher->getDecrypted());

        if (!$hasher->getDecrypted()) {
            $output->writeln('Hola amigo, provided credentials didnt open this box!');
        }
    }

    /**
     * Initialize for input
     */
    protected function initialize(InputInterface $input, OutputInterface $output)
    {
        $this->io = new SymfonyStyle($input, $output);
        $this->io->title('Silesiacoin\'s Command Sodium Secretbox Encryption');
    }

    /**
     * Interact with user
     * @throws \Symfony\Component\Console\Exception\InvalidArgumentException
     */
    protected function interact(InputInterface $input, OutputInterface $output)
    {
        foreach ($input->getOptions() as $option => $value) {
            if (null === $value) {
                $input->setOption($option, $this->io->ask(sprintf('%s', ucfirst($option))));
            }
        }
    }
}
