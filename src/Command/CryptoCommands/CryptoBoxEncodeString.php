<?php
declare(strict_types=1);

namespace App\Command\CryptoCommands;

use Silesiacoin\Phplib\Sodium\SecretBox;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;
use Symfony\Component\Filesystem\Filesystem;

class CryptoBoxEncodeString extends ContainerAwareCommand
{
    /** @var SymfonyStyle */
    private $io;

    /**
     * UserAdd constructor.
     * @throws \Exception
     */
    public function __construct()
    {
        parent::__construct('sodium:secretbox:encode');
    }

    protected function configure() : void
    {
        $this
            ->addOption(
                'Nonce',
                null,
                InputOption::VALUE_REQUIRED,
                'provide filename'
            )
            ->addOption(
                'Password',
                null,
                InputOption::VALUE_REQUIRED,
                'provide hashed password'
            )
            ->addOption(
                'Message',
                null,
                InputOption::VALUE_OPTIONAL,
                'provide message'
            )
            ->addOption(
                'FileName',
                null,
                InputOption::VALUE_REQUIRED,
                'Provide filename',
                'temp/secret.ssc'
            )
            ->addOption(
                'Bits',
                null,
                InputOption::VALUE_OPTIONAL,
                'provide Bit size',
                16
            )
            ->setDescription('provide nonce')
            ->setHelp(
                'This command allows you to store secretBox in file.
                Key is like Password, Nonce is like Login. This values must be unique.
                --FileName option flag redirects you to file dir dump destination'
            );
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     * @return int|null|void
     * @throws \Symfony\Component\DependencyInjection\Exception\ServiceNotFoundException
     * @throws \Symfony\Component\DependencyInjection\Exception\ServiceCircularReferenceException
     * @throws \LogicException
     * @throws \InvalidArgumentException
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $fs = new Filesystem();
        $hasher = new SecretBox($input->getOption('Password'), $input->getOption('Nonce'), $input->getOption('Bits'));
        $hasher->setPaddedMessage($input->getOption('Message'));
        $hasher->encodeString();
        $hasher->strKey = $hasher->binToStr($hasher->getBinKey());
        $hasher->strNonce = $hasher->binToStr($hasher->getBinNonce());

        $fs->dumpFile($input->getOption('FileName'), $hasher->getBinStrMessage());

        $output->writeln('Secret in: ' . $input->getOption('FileName'));
    }

    /**
     * Initialize for input
     */
    protected function initialize(InputInterface $input, OutputInterface $output)
    {
        $this->io = new SymfonyStyle($input, $output);
        $this->io->title('Silesiacoin\'s Command Sodium Secretbox Encryption');
    }

    /**
     * Interact with user
     */
    protected function interact(InputInterface $input, OutputInterface $output)
    {
        foreach ($input->getOptions() as $option => $value) {
            if (null === $value) {
                $input->setOption($option, $this->io->ask(sprintf('%s', ucfirst($option))));
            }
        }
    }
}
